import pandas as pd
import numpy as np

import argparse
import csv
import json
import logging
import os

import torch

from data.util import NumpyEncoder

# Area unit columns
ELECTORAL_DISTRICT = "GED2007_label"
AREA_UNIT = "AU2013_label"
AREA_UNIT_CODE = "AU2013_code"
TERITORIAL_AUTHORITY = "TA2010_label"

# Demographic tables
AGE = "age.tsv"
GENDER = "gender.tsv"
ETHNIC_GROUP = "ethnicity.tsv"
#LANGUAGE = "language.csv"
RELIGION = "religion.tsv"
PARTNERSHIP = "partnership.tsv"
#CHILDREN = "children.csv"
#QUALIFICATION = "qualification.csv"
INCOME = "income.tsv"
#EMPLOYMENT = "employment.csv"
HOUSEHOLD_COMPOSITION = "household.tsv"
VEHICLES = "vehicles.tsv"

class NewZealandPopulation:

    def __init__(self, census_directory, transition_matrix_file, area_units_csv):

        self.transition_matrix, self.index_to_region_map = parse_nz_transition_matrix(transition_matrix_file)

        self.area_unit_map = parse_area_unit_map(area_units_csv)
        self.area_name_to_code = parse_area_unit_map(area_units_csv, AREA_UNIT, AREA_UNIT_CODE)
        self.distributions = {}
        self.labels = {}

        for categorical_feature in [AGE, GENDER, ETHNIC_GROUP, RELIGION, PARTNERSHIP, INCOME, HOUSEHOLD_COMPOSITION, VEHICLES]:
            feature_csv = "{}/{}".format(census_directory, categorical_feature)

            if os.path.exists(feature_csv):
                distribution_map, columns = parse_general_electorate_distributions(feature_csv)
                for key in distribution_map:
                    if key not in self.distributions:
                        self.distributions[key] = {}
                    self.distributions[key][categorical_feature] = distribution_map[key]
                self.labels[categorical_feature] = columns
            else:
                logging.warning("File {} is missing.".format(feature_csv))

        self.distributions = map_features(self.area_unit_map, self.distributions)

    @classmethod
    def build(cls, population_file):
        instance = cls.__new__(cls)
        instance.load(population_file)
        return instance

    def save(self, population_file):
        population_json = {
            "transition_matrix": self.transition_matrix,
            "index_to_region_map": self.index_to_region_map,
            "area_unit_map": self.area_unit_map,
            "area_name_to_code": self.area_name_to_code,
            "distributions": self.distributions,
            "labels": self.labels
        }

        with open(population_file, "w") as f:
            json.dump(population_json, f, cls=NumpyEncoder)

    def load(self, population_file):
        with open(population_file, "r") as f:
            population_json = json.load(f)

        self.transition_matrix = population_json["transition_matrix"]
        self.index_to_region_map = population_json["index_to_region_map"]
        self.area_unit_map = population_json["area_unit_map"]
        self.area_name_to_code = population_json["area_name_to_code"]
        self.distributions = population_json["distributions"]
        self.labels = population_json["labels"]

    def compute_ranges(self):
        '''
        TODO: handle numerical feaure indexes
        :return:
        '''
        feature_indices = [0]
        feature_index = 0
        for feature_id in self.labels:
            feature_index += len(self.labels[feature_id])
            feature_indices.append(feature_index)

        return feature_indices

    def sample(self, suppress_warnings=True):
        '''
        Generates a population sample with size equal to the transition matrix length
        Currently assumes categorical distributions
        :return:
        '''

        feature_probabilities = {} # holds feature probabilities for if we need to impute missing values
        feature_width = self.compute_ranges()[-1]

        population_samples = np.zeros(shape=(len(self.index_to_region_map), feature_width))

        for index in self.index_to_region_map:
            try:
                region = self.index_to_region_map[int(index)]
            except KeyError:
                region = self.index_to_region_map[str(index)]

            if region in self.distributions:
                features = self.distributions[region]

                sample = []
                for feature_id in self.labels:
                    feature_sample = [0 for _ in range(len(self.labels[feature_id]))]

                    if feature_id in features:
                        feature_probabilities[feature_id] = features[feature_id]
                    else:
                        if not suppress_warnings:
                            logging.warning("Feature Key: {} not found for region: {}".format(feature_id, region))

                    random_choice = np.random.choice(range(len(feature_probabilities[feature_id])), p=feature_probabilities[feature_id])
                    feature_sample[random_choice] += 1
                    sample += feature_sample

            else:
                sample = population_samples[int(index)-1]
                if not suppress_warnings:
                    logging.warning("Region Key: {} not found in distribution map".format(region))

            population_samples[int(index)] += sample

        return population_samples

    def write_area_unit_population_csv(self, output_location, sample_count=10000, encoder=None):
        population_count = 0
        columns = [k for k, _ in self.labels.items()]
        header = ["origin"] + [key.replace('.tsv', '') for key in columns]
        print(header)
        rows = []
        feature_indices = self.compute_ranges()

        if encoder:
            embeddings = []
            encoder.eval()

        while population_count < sample_count:
            population_sample = self.sample()

            if encoder:
                population_embeddings, _ = encoder.encode(torch.Tensor(population_sample))
                population_embeddings = population_embeddings.detach().numpy().tolist()
            for index in range(len(population_sample)):
                row = []
                try:
                    region = self.index_to_region_map[int(index)]
                except KeyError:
                    region = self.index_to_region_map[str(index)]

                if region in self.area_name_to_code:
                    area_code = self.area_name_to_code[region]
                    row.append(area_code)
                    population_count += 1
                    person = population_sample[index]

                    # slice and select feature name with this hackity hack
                    column_index = 0
                    prev_feature_index = 0
                    for feature_index in feature_indices[1:]:
                        sample_feature_index = np.argmax(person[prev_feature_index:feature_index])
                        feature_name = self.labels[columns[column_index]][sample_feature_index]
                        prev_feature_index = feature_index
                        column_index += 1
                        row.append(feature_name)

                    rows.append(row)
                    if encoder:
                        embeddings.append(population_embeddings[index])

                    if population_count < 5:
                        print(row)
                        if encoder:
                            print(population_embeddings[index])

        with open(output_location, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t')
            writer.writerow(header)
            for row in rows:
                writer.writerow(row)

        if encoder:
            embedding_output = output_location.replace('.tsv', '_embeddings.tsv')
            with open(embedding_output, 'w') as embedding_csvfile:
                writer = csv.writer(embedding_csvfile, delimiter='\t')
                for embedding in embeddings:
                    writer.writerow(embedding)


def parse_nz_transition_matrix(transition_matix_csv, buffer_columns=2):
    '''
    TODO: Why is this creating NaNs
    :param transition_matix_csv: source csv for transition matrix
    :param buffer_columns: number of columns to buffer before first transition in each row
    :return:
    '''
    region_to_index_map = {}
    df = pd.read_csv(transition_matix_csv, header=0, sep="\t")

    columns = df.columns.values[buffer_columns:]

    for column_name in columns:
        region_to_index_map[column_name] = df.columns.get_loc(column_name)-buffer_columns

    index_to_region_map = {value: key for key, value in region_to_index_map.items()}

    transition_matrix = np.zeros(shape=(len(index_to_region_map), len(index_to_region_map)))

    for row_number, row in df.iterrows():
        for column in columns:
            column_number = region_to_index_map[column]
            try:
                transition_matrix[row_number, column_number] = row[column]
            except ValueError:
                transition_matrix[row_number, column_number] = 0

    return transition_matrix, index_to_region_map

def parse_area_unit_map(area_units_csv, geographic_unit=AREA_UNIT,map_to_column=ELECTORAL_DISTRICT):
    area_unit_map = {}
    df = pd.read_csv(area_units_csv, header=0)

    columns = df.columns.values

    if geographic_unit not in columns:
        raise ValueError("The geographic unit {} is not a valid column for the passed csv {}"
                         .format(geographic_unit, area_units_csv))

    if map_to_column not in columns:
        raise ValueError("The mapping column {} is not a  valid column for the passed csv {}"
                         .format(map_to_column, area_units_csv))

    for id, row in df.iterrows():
        area_unit_map[row[geographic_unit]] = row[map_to_column]

    return area_unit_map

def parse_general_electorate_distributions(feature_csv, padding_columns=0):
    print("Reading file {}".format(feature_csv))
    df = pd.read_csv(feature_csv, header=0, thousands=',', sep="\t")

    columns = df.columns.values
    length = len(columns) - padding_columns
    columns = columns[1:length]
    general_electorate = df.columns.values[0] # assume

    distribution_map = {}

    for row_number, row in df.iterrows():
        general_electorate_id = row[general_electorate]
        features = []
        for column in columns:
            try:
                features.append(int(row[column]))
            except ValueError:
                features.append(0)
        total = max(sum(features), 1)
        distribution_map[general_electorate_id] = [x/total for x in features]

    return distribution_map, columns

def map_features(geographical_region_map, distribution_map):
    '''
    :param geographical_region_map: a -> b
    :param distribution_map:  b-> [features]
    :return: distribution map based on new geographical regions
    '''

    new_region_distribution_map = {}
    for key in geographical_region_map:
        try:
            new_region_distribution_map[key] = distribution_map[geographical_region_map[key]]
        except KeyError:
            try:
                geographical_key = geographical_region_map[key]
                logging.warning("Key: {} was not found in distribution map".format(geographical_key))
            except KeyError:
                logging.warning("Key: {} was not recognized in geographical region map.".format(key))


    return new_region_distribution_map

def parse_population_args():
    parser = argparse.ArgumentParser(description='Bitsim Swarms for early stopping on AWS step functions')
    parser.add_argument("-a", "--area_units_csv", type=str, help='The path to the area units map csv.',
                        default="data/nz_data/nz_area_units_2013.csv")
    parser.add_argument("-c", "--census_directory", type=str, help="The directory for census feature files",
                        default="data/nz_data/usable_electoral_tables_2013")
    parser.add_argument("-o", "--output_json", type=str, help="The output file to write the population_json",
                        default="data/nz_data/population.json")
    parser.add_argument("-t", "--transition_matrix_file", type=str, help="The Transition matrix file.",
                        default="data/nz_data/2013-usual-residence-by-workplace-address-area-unit.tsv")
    parser.add_argument("-w", "--write_csv", type=str, help="The output csv to write a population sample",
                        default="data/nz_data/population_sample.csv")
    return parser.parse_args()

if __name__ == "__main__":
    arguments = parse_population_args()
    new_zealand_population = NewZealandPopulation(arguments.census_directory,
                                                  arguments.transition_matrix_file,
                                                  arguments.area_units_csv)
    new_zealand_population.save(arguments.output_json)
    sample = new_zealand_population.sample()
    print(np.array(sample))
    print(len(sample))
    print(len(sample[0]))

    new_zealand_population.write_area_unit_population_csv(arguments.write_csv, sample_count=1000000)