import unittest

from data.new_zealand_population import *

TEST_CENSUS_DIR = "tests/fixtures"
TEST_AGE_CSV = "tests/fixtures/age.tsv"
TEST_AREA_UNITS_CSV = "tests/fixtures/test_area_units.csv"
TEST_TRANSITION_MATRIX_CSV = "tests/fixtures/test_transition_matrix.csv"

class TestNewZealandPopulation(unittest.TestCase):
    def test_parse_nz_transition_matrix_csv(self):
        expected_transition_matrix = [[5., 0., 5.], [4., 6., 0.], [1., 4., 5.]]
        expected_index_to_region_map = {0: 'Tidal-Tamaki', 1: 'Auckland City-Marinas', 2: 'Hairini'}
        for i in range(10):
            output_transition_matrix, output_index_to_region_map = parse_nz_transition_matrix(TEST_TRANSITION_MATRIX_CSV)
            self.assertListEqual(output_transition_matrix.tolist(), expected_transition_matrix)
            self.assertDictEqual(output_index_to_region_map, expected_index_to_region_map)

    def test_parse_area_unit_map_csv(self):
        expected_area_unit_map = {'Auckland City-Marinas': 'Auckland Central', 'Hairini': 'Bay of Plenty',
                                  'Tidal-Tamaki': 'Botany'}
        for i in range(10):
            output_area_unit_map = parse_area_unit_map(TEST_AREA_UNITS_CSV)
            self.assertDictEqual(output_area_unit_map, expected_area_unit_map)

    def test_parse_general_electorate_age(self):
        expected_columns = ['0–4 Years', '5–9 Years', '10–14 Years', '15–19 Years', '20–24 Years',
                            '25–29 Years', '30–34 Years', '35–39 Years', '40–44 Years', '45–49 Years',
                            '50–54 Years', '55–59 Years', '60–64 Years', '65–69 Years', '70–74 Years',
                            '75–79 Years', '80–84 Years', '85 Years And Over']
        output_distributions, output_columns = parse_general_electorate_distributions(TEST_AGE_CSV)

        for key in output_distributions:
            self.assertEqual(len(output_columns), len(output_distributions[key]))
            self.assertAlmostEqual(sum(output_distributions[key]), 1)

        self.assertListEqual(output_columns.tolist(), expected_columns)

    def test_construct_new_zealand_population(self):
        expected_columns = ['0–4 Years', '5–9 Years', '10–14 Years', '15–19 Years', '20–24 Years',
                            '25–29 Years', '30–34 Years', '35–39 Years', '40–44 Years', '45–49 Years',
                            '50–54 Years', '55–59 Years', '60–64 Years', '65–69 Years', '70–74 Years',
                            '75–79 Years', '80–84 Years', '85 Years And Over']

        nzp = NewZealandPopulation(TEST_CENSUS_DIR, TEST_TRANSITION_MATRIX_CSV, TEST_AREA_UNITS_CSV)

        for key in nzp.distributions:
            for sub_key in nzp.distributions[key]:
                self.assertAlmostEqual(sum(nzp.distributions[key][sub_key]), 1)

        self.assertListEqual(nzp.labels['age.tsv'].tolist(), expected_columns)

    def test_map_features(self):
        area_unit_map = {'Auckland City-Marinas': 'Auckland Central', 'Hairini': 'Bay of Plenty',
                         'Tidal-Tamaki': 'Botany'}
        distributions, _ = parse_general_electorate_distributions(TEST_AGE_CSV)

        output_distributions = map_features(area_unit_map, distributions)

        for key in area_unit_map:
            self.assertIn(key, output_distributions.keys())