import numpy as np
import torch
import logging

from loaders.abstract_loader import AbstractLoader

class GraphSampleLoader(AbstractLoader):

	def __init__(self, data_source, batch_size, adjacency=True):
		'''
		:param data_source: A population
		:param batch_size: The number of nodes in the graph
		:param graph_source: The graph adjacency matrix
		'''
		super(GraphSampleLoader, self).__init__(data_source, batch_size)

		self.adjacency_matrix = torch.Tensor(self.load_transition_matrix())
		if torch.sum(torch.isnan(self.adjacency_matrix)) > 0:
			print("The adjacency matrix contains {} Nans".format(torch.sum(torch.isnan(self.adjacency_matrix))))
		self.link_matrix = torch.clamp(torch.Tensor(self.adjacency_matrix), 0, 1)
		print(self.link_matrix)
		if torch.sum(torch.isnan(self.link_matrix)) > 0:
			print("The link matrix contains {} Nans".format(torch.sum(torch.isnan(self.link_matrix))))
		if adjacency:
			self.graph_laplacian = compute_normalized_adjacency(self.link_matrix)
		else:
			self.graph_laplacian = compute_graph_laplacian(self.link_matrix)

		print(self.graph_laplacian)

	def load(self):
		'''
		:return: sample of X, y
		'''
		return np.array(self.data_source.sample())

	def load_transition_matrix(self):
		tm = np.array(self.data_source.transition_matrix)
		nan_mask = np.isnan(tm)
		logging.warning("There are {} NaNs in the transition matrix. They will be replaced with 0.".format(np.sum(nan_mask)))
		tm[nan_mask] = 0
		return tm

def compute_normalized_adjacency(adjacency_matrix):
	adjacency_tensor = torch.Tensor(adjacency_matrix)
	return 2*torch.eye(adjacency_tensor.size()[0]) - compute_graph_laplacian(adjacency_matrix)

def compute_graph_laplacian(adjacency_matrix, epsilon=1e-3):
	adjacency_tensor = torch.Tensor(adjacency_matrix)
	(width, height) = adjacency_tensor.size()
	laplacian = torch.eye(adjacency_tensor.size()[0])
	in_degree = torch.sum(adjacency_tensor, dim=1)
	out_degree = torch.sum(adjacency_tensor, dim=0)
	degree_matrix = torch.eye(adjacency_tensor.size()[0]) * out_degree

	#slow and steady, but it is the only way to keep NaN out of your life
	for i in range(width):
		for j in range(height):
			if i != j and adjacency_tensor[i, j] > 0 and degree_matrix[i,i] > 0 and degree_matrix[j,j] > 0:
				laplacian[i, j] -= 1 / torch.sqrt(degree_matrix[i,i] * degree_matrix[j,j]) + epsilon

	return laplacian