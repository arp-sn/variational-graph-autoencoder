''' Copyright (C) Sean Billings - All Rights Reserved
 	Unauthorized copying of this file, via any medium is strictly prohibited
 	Proprietary and confidential
 	Written by Sean Billings <s.a.f.billings@gmail.com>, August 2019
'''

import torch

from tqdm import tqdm
from ml_kit.trainers.abstract_trainer import AbstractTrainer

class GraphModelTrainer(AbstractTrainer):
	def __init__(self, model, criterion, optimizer, device):
		super(GraphModelTrainer, self).__init__(model, criterion, optimizer, device)
		self.batch = 0
		self.epoch = 0

	# helper function for training
	def train(self, loader, num_batches, mega_batch=False, clip=1):
		'''
        :param loader: AbstractLoader Implementation
        :param num_batches: number of batches to call
        :return:
        '''

		self.model.train()
		self.optimizer.zero_grad()
		total_loss = torch.zeros(1, requires_grad=True)
		categorical_loss = torch.zeros(1, requires_grad=False)
		numerical_loss = torch.zeros(1, requires_grad=False)
		link_loss = torch.zeros(1, requires_grad=False)
		kl_loss = torch.zeros(1, requires_grad=False)
		embeddings = []
		for i in tqdm(range(num_batches), desc='Training Epoch ' + str(self.epoch) + ' '):

			population_output, link_prediction, losses, kl_l, embedding = self.get_model_output(loader)
			total_loss = total_loss + losses[0] + kl_l
			categorical_loss = categorical_loss + losses[1]
			numerical_loss = numerical_loss + losses[2]
			link_loss = link_loss + losses[3]
			kl_loss = kl_loss + kl_l

			embeddings.append(embedding)
			self.batch += 1
			if not mega_batch:
				losses[0].backward()
				self.optimizer.step()

		total_loss = total_loss / num_batches
		categorical_loss /= num_batches
		numerical_loss /= num_batches
		link_loss /= num_batches
		kl_loss /= num_batches
		if mega_batch:
			total_loss.backward()
			self.optimizer.step()

		self.epoch += 1

		return total_loss, categorical_loss, numerical_loss, link_loss, kl_loss, embeddings

	def test(self, loader, num_batches):
		self.model.eval()
		with torch.no_grad():
			total_loss = 0.0
			population = []
			links = []
			embeddings = []

			for _ in tqdm(range(num_batches), desc='Testing batches ' + str(num_batches) + ''):
				population_output, link_prediction, losses, batch_embeddings = self.get_model_output(loader)
				total_loss += losses[0].item()

				for output in population_output:
					population.append(torch.argmax(output).item())

				for link in link_prediction:
					links.append(torch.argmax(link))

				for embedding in batch_embeddings:
					embeddings.append(embedding)

			total_loss /= num_batches
			population = torch.as_tensor(population, dtype=torch.float, device=self.device)
			links = torch.as_tensor(links, dtype=torch.int, device=self.device)

			return total_loss, population, links, embeddings

	def get_model_output(self, loader):
		graph_batch = loader.load()
		graph_batch = torch.as_tensor(graph_batch, dtype=torch.float).to(self.device)

		population_output, link_prediction, graph_embedding, kl_loss = self.model(graph_batch)
		losses = self.criterion(population_output, link_prediction, graph_batch, loader.link_matrix)
		return population_output, link_prediction, losses, kl_loss, graph_embedding