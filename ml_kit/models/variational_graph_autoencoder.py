import torch
import torch.nn as nn
import logging

from ml_kit.models.layers.graph_convolution import GraphConvolution

class VariationalGraphAutoencoder(nn.Module):

	def __init__(self, input_width, embedding_size, output_size, normalized_laplacian, variation=None, do_log=False, device='cpu'):
		super(VariationalGraphAutoencoder, self).__init__()

		# define layers
		self.device = device

		self.embedding_size = embedding_size

		self.normalized_laplacian = normalized_laplacian
		self.normalized_laplacian.requires_grad = False

		if torch.sum(torch.isnan(normalized_laplacian)) > 0:
			logging.warning("The Adjacency Matrix contains {} Nans".format(torch.sum(torch.isnan(normalized_laplacian))))

		self.normalized_transpose = self.normalized_laplacian.T
		self.normalized_transpose.requires_grad = False

		self.first_graph_convolution = GraphConvolution(input_width, embedding_size)

		self.second_graph_convolution = GraphConvolution(embedding_size, embedding_size)

		self.variation = variation

		self.normal_distribution = torch.distributions.multivariate_normal.MultivariateNormal(torch.zeros(1), torch.eye(1))

		if self.variation:
			self.variance_graph_convolution = GraphConvolution(embedding_size, embedding_size)
		else:
			self.variance_graph_convolution = None

		self.population_decoder = nn.Linear(embedding_size, input_width)
		torch.nn.init.xavier_uniform_(self.population_decoder.weight)

		self.plan_decoder = nn.Linear(embedding_size, output_size)
		torch.nn.init.xavier_uniform_(self.plan_decoder.weight)

		self.do_log = do_log

	def toggle_variation(self):
		if self.variance_graph_convolution is None:
			self.variance_graph_convolution = GraphConvolution(self.embedding_size, self.embedding_size)
			self.variance_graph_convolution.weight.data = self.second_graph_convolution.weight.data.clone()
			self.variance_graph_convolution.bias.data = self.second_graph_convolution.bias.data.clone()
			self.variance_graph_convolution.requires_grad = True

		if self.normal_distribution is None:
			self.normal_distribution = torch.distributions.multivariate_normal.MultivariateNormal(torch.zeros(1), torch.eye(1))

		self.variation = True

	def forward(self, batch):
		graph_embedding, kl_loss = self.encode(batch, variation=self.variation)
		population_output, link_prediction = self.predict(graph_embedding)
		return population_output, link_prediction, graph_embedding, kl_loss

	def encode(self, batch, variation=False):
		if self.do_log and torch.sum(torch.isnan(batch)) > 0:
			logging.warning("The Batch contains {} Nans".format(torch.sum(torch.isnan(batch))))

		first_graph_embedding = nn.ReLU()(self.first_graph_convolution(batch, self.normalized_laplacian))
		if self.do_log and torch.sum(torch.isnan(first_graph_embedding)) > 0:
			logging.warning(
				"First graph convolution is causing {} NaNs".format(torch.sum(torch.isnan(first_graph_embedding))))

		second_graph_embedding = nn.ReLU()(
			self.second_graph_convolution(first_graph_embedding, self.normalized_transpose))
		if self.do_log and torch.sum(torch.isnan(second_graph_embedding)) > 0:
			logging.warning("Second graph convolution is causing NaNs")

		if variation:
			mu_graph_embedding = second_graph_embedding
			root_sigma_graph_embedding = self.variance_graph_convolution(first_graph_embedding,
																		 self.normalized_transpose)
			normal_epsilon = self.normal_distribution.sample()
			full_sigma = torch.einsum('ni,nj->nij', root_sigma_graph_embedding, root_sigma_graph_embedding)
			graph_embedding = mu_graph_embedding + (root_sigma_graph_embedding / 2) * normal_epsilon

			mean_drift = torch.sum(torch.einsum('ni,ni->n', mu_graph_embedding, mu_graph_embedding))
			variance_drift = torch.trace(torch.sum(full_sigma, dim=0))
			log_sigma = torch.log(full_sigma)

			# haddmards inequality for positive def hermitian matrices
			log_determinant_approx = torch.trace(torch.sum(log_sigma, dim=0))

			kl_loss = 0.5 * variance_drift + mean_drift - log_determinant_approx - full_sigma.size(
				0) * full_sigma.size(1)
			# see tutorial on variational autoencoder page 9 https://arxiv.org/pdf/1606.05908.pdf
			if kl_loss < 0:
				kl_loss = 0
		else:
			graph_embedding = second_graph_embedding
			kl_loss = torch.zeros(1)

		return graph_embedding, kl_loss

	def predict(self, embedding):
		population_output = self.population_decoder(embedding)
		if self.do_log and torch.sum(torch.isnan(population_output)) > 0:
			logging.warning("Population Output is causing NaNs")

		plan_output = self.plan_decoder(embedding)
		if self.do_log and torch.sum(torch.isnan(plan_output)) > 0:
			logging.warning("Plan Output is causing NaNs")

		plan_output = nn.ReLU()(plan_output)
		plan_outer_product = torch.einsum('ik,jk->ij', plan_output, plan_output)
		if self.do_log and torch.sum(torch.isnan(plan_outer_product)) > 0:
			logging.warning("Plan Outer Product is causing NaNs")

		link_prediction = nn.Sigmoid()(plan_outer_product)
		if self.do_log and torch.sum(torch.isnan(link_prediction)) > 0:
			logging.warning("Link Prediction is causing NaNs")

		return population_output, link_prediction

	def generate(self):
		sample_shape = torch.Size([len(self.normalized_laplacian), self.embedding_size])
		embedding_sample = self.normal_distribution.sample(sample_shape=sample_shape) * torch.ones(size=sample_shape)
		population_output, link_prediction = self.predict(embedding_sample)
		return embedding_sample, population_output, link_prediction

	def save(self, path):
		torch.save(self.state_dict(), path)

	def load(self, path):
		self.load_state_dict(torch.load(path))