import argparse
import numpy as np
import torch
import matplotlib.pyplot as plt

from data.new_zealand_population import NewZealandPopulation

from ml_kit.loaders.graph_sample_loader import GraphSampleLoader
from ml_kit.models.variational_graph_autoencoder import VariationalGraphAutoencoder
from ml_kit.trainers.graph_model_trainer import GraphModelTrainer
from ml_kit.metrics.graph_model_criterion import GraphModelCriterion, categorical_cross_entropy, matrix_l2_norm

from torch.nn import CrossEntropyLoss, MSELoss, BCELoss

def parse_model_args():
    parser = argparse.ArgumentParser(description='Bitsim Swarms for early stopping on AWS step functions')
    parser.add_argument("-d", "--embedding_dimension", type=int, help='The embedding size for the VGA',
                        default=500)
    parser.add_argument("-e", "--epochs", type=int, help='The number of epochs to train for',
                        default=5)
    parser.add_argument("-i", "--iterations", type=int, help='The number of iterations to generate per epoch',
                        default=32)
    parser.add_argument("-o", "--output_size", type=int, help='The output size for the VGA',
                        default=5)
    parser.add_argument("-m", "--mega_batch", type=int, help='Decide whether to perform mega batching',
                        default=0)
    parser.add_argument("-p", "--population_file", type=str, help='The serialized population',
                        default="data/nz_data/population.json")
    parser.add_argument("-v", "--variation", type=int, help='Vary, or not vary',
                        default=0)
    parser.add_argument("-w", "--write", type=str, help='Where to write, if at all',
                        default='')
    return parser.parse_args()

if __name__ == "__main__":
    arguments = parse_model_args()

    new_zealand_population = NewZealandPopulation.build(arguments.population_file)

    sample = np.array(new_zealand_population.sample())
    input_width = len(sample[0])
    batch_size = len(sample)

    loader = GraphSampleLoader(new_zealand_population, batch_size)

    model = VariationalGraphAutoencoder(input_width=input_width,
                                        embedding_size=arguments.embedding_dimension,
                                        output_size=arguments.output_size,
                                        normalized_laplacian=loader.graph_laplacian,
                                        device='cpu')

    criterion = GraphModelCriterion(categorical_indices=new_zealand_population.compute_ranges(),
                                    numerical_indices=[],
                                    link_loss=BCELoss())

    optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()),
                                lr=0.5, weight_decay=1e-3)

    trainer = GraphModelTrainer(model=model,
                                criterion=criterion,
                                optimizer=optimizer,
                                device='cpu')
    embeddings = []
    losses = []
    kl_losses = []
    link_losses = []
    categorical_losses = []
    for e in range(arguments.epochs):
        print([ (key, torch.mean(value)) for (key, value) in trainer.model.state_dict().items()])
        loss, categorical_loss, numerical_loss, link_loss, kl_loss, embedding = trainer.train(loader,
                                                                                              arguments.iterations,
                                                                                              mega_batch=arguments.mega_batch)
        print("\nTraining loss: {0:.4f}\n".format(loss.item()))
        print("Categorical loss: {0:.4f}".format(categorical_loss.item()))
        print("Numerical loss: {0:.4f}".format(numerical_loss.item()))
        print("Link loss: {0:.4f}".format(link_loss.item()))
        print("KL loss: {0:.4f}\n".format(kl_loss.item()))
        losses.append(categorical_loss.item()+link_loss.item())
        categorical_losses.append(categorical_loss.item())
        kl_losses.append(kl_loss.item())
        link_losses.append(link_loss.item())
        embeddings.append(embedding)

    # variance update
    if arguments.variation:
        model.toggle_variation()
        trainer.model = model # I shouldn't need to do this
        optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()),
                                     lr=0.5, weight_decay=1e-3)
        trainer.optimizer = optimizer

        for e in range(arguments.epochs):
            print([(key, torch.mean(value)) for (key, value) in trainer.model.state_dict().items()])
            loss, categorical_loss, numerical_loss, link_loss, kl_loss, embedding = trainer.train(loader,
                                                                                                  min(4,arguments.iterations),
                                                                                                  mega_batch=arguments.mega_batch)
            print("\nTraining loss: {0:.4f}\n".format(loss.item()))
            print("Categorical loss: {0:.4f}".format(categorical_loss.item()))
            print("Numerical loss: {0:.4f}".format(numerical_loss.item()))
            print("Link loss: {0:.4f}".format(link_loss.item()))
            print("KL loss: {0:.4f}\n".format(kl_loss.item()))
            losses.append(categorical_loss.item()+link_loss.item())
            categorical_losses.append(categorical_loss.item())
            kl_losses.append(kl_loss.item())
            link_losses.append(link_loss.item())
            embeddings.append(embedding)

    if len(arguments.write) > 0:
        new_zealand_population.write_area_unit_population_csv(arguments.write, encoder=trainer.model)

    x = [i for i in range((len(losses)))]
    tl, = plt.plot(losses, label='Training loss')
    cl, = plt.plot(categorical_losses, label='Categorical_Loss')
    kl, = plt.plot(kl_losses, label='KL loss')
    ll, = plt.plot(link_losses, label='Link loss')
    fit, = plt.plot(np.unique(x), np.poly1d(np.polyfit(x, losses, 1))(np.unique(x)), label='Training Loss Trend')
    plt.legend(handles=[tl,cl,kl,ll,fit])
    plt.yscale('log')
    plt.show()



