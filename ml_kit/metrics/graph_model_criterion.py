import torch
import logging

from torch.nn import CrossEntropyLoss, MSELoss, BCELoss

def categorical_cross_entropy(predictions, targets):
    log_predictions = torch.log(predictions)
    product = -1*log_predictions*targets
    loss = torch.sum(product)
    return loss

def matrix_l2_norm(predictions, targets):
    if torch.sum(torch.isnan(predictions)) > 0:
        logging.warning("Predictions contains {} Nans".format(torch.sum(torch.isnan(predictions))))
    if torch.sum(torch.isnan(targets)) > 0:
        logging.warning("Targets contains {} Nans".format(torch.sum(torch.isnan(targets))))

    difference = predictions - targets
    if torch.sum(torch.isnan(difference)) > 0:
        logging.warning("The difference contains {} Nans".format(torch.sum(torch.isnan(difference))))

    loss = difference.norm()
    return loss

class GraphModelCriterion:

    def __init__(self, categorical_indices, numerical_indices,
                 categorical_loss=CrossEntropyLoss(), numerical_loss=MSELoss(), link_loss=matrix_l2_norm,
                 epsilon=1e-3, do_log=False):
        self.numerical_indices = numerical_indices # not really used yet

        self.categorical_ranges = []
        for i in range(len(categorical_indices) - 1):
            self.categorical_ranges.append((categorical_indices[i], categorical_indices[i+1]))

        self.categorical_loss = categorical_loss
        self.numerical_loss = numerical_loss
        self.link_loss = link_loss
        self.epsilon = epsilon

        self.do_log = do_log

    def __call__(self, population_output, link_prediction, graph_batch, adjacency_matrix):
        loss = torch.zeros(1, requires_grad=True)

        categorical_l = 0
        numerical_l = 0
        link_l = 0

        for range in self.categorical_ranges:
            start = range[0]
            end = range[1]
            target_slice = population_output[:, start:end]
            batch_slice = graph_batch[:, start:end]
            _, target_indices = target_slice.max(dim=1)
            categorical_l = categorical_l + self.categorical_loss(batch_slice, target_indices)

        loss = loss + categorical_l

        for index in self.numerical_indices:
            output_slice = population_output[:, index:index+1]
            batch_slice = graph_batch.narrow[:, index:index+1]
            numerical_l = numerical_l + self.numerical_loss(output_slice, batch_slice)

        loss = loss + numerical_l

        target_matrix = torch.Tensor(adjacency_matrix)
        nan_mask = ~torch.isnan(link_prediction)
        links = link_prediction[nan_mask]
        if self.do_log and torch.sum(torch.isnan(links)) > 0:
            logging.warning("Pruned Links contains {} Nans".format(torch.sum(torch.isnan(links))))
        targets = target_matrix[nan_mask]

        clamped_links = torch.clamp(links, self.epsilon, 1 - self.epsilon).flatten()

        if self.do_log and torch.sum(torch.isnan(clamped_links)) > 0:
            logging.warning("Clamped Links contains {} Nans".format(torch.sum(torch.isnan(clamped_links))))

        link_l = self.link_loss(clamped_links, targets.flatten())
        loss = loss + link_l

        return (loss, categorical_l, numerical_l, link_l)
